<?php

namespace app\webview\controller;

// use Vipkwd\Utils\Wx\Mp\Message\Template as MpTemplate;
// use Vipkwd\Utils\Image\Qrcode as VkQrcode;
// use Vipkwd\Utils\Image\Thumb as VkThumb;
// use Vipkwd\Utils\Ip as VkIP;
// use Vipkwd\Utils\Wx\Mp\User\User as MpUser;
// // use Vipkwd\Utils\SocketPush\WebPusher;
use Vipkwd\Utils\Scan\WebLogin;

use app\BaseController;

/**
 * 默认控制器
 * Class Qrcode
 * @package app\webview\controller
 */
class WebLoginDemo extends BaseController
{

    private function instance()
    {
        return new WebLogin([
            'mp_app_name' => '',
            'mp_app_id' => '',
            'mp_app_secret' => '',
            'web_push_url' => '',
            'signFields' => ['userId', 'clientId', 'qrcodeId'],
        ]);
    }

    //[绑定账户] 生成二维码
    public function bindQrcodeimage()
    {
        /**
         * userId
         * clientId
         * qrcodeId
         */
        $this->instance()->bindQrcodeCreate([
            'redirect' => '/webview/demo/bindQrcodeValidator'
        ]);
    }

    //[绑定账户] 微信扫码后跳转的页面方法
    public function bindQrcodeValidator()
    {
        $fetchLocalUserOpenIdWithUid = function ($userId) {
            //TODO 返回库中 用户的公众号OpenID
            return '222222';
        };
        $result = $this->instance()->bindQrcodeValidate($fetchLocalUserOpenIdWithUid);

        //俩个场景会返回数组:
        // 1 、第一次关注公众号 + 绑定账号后，又取消了“公众号关注”
        // 2 、历史没有关注过公众号（本地没有绑定记录）
        if (is_array($result)) {
            /*
                'openId' => $res['openid'],
                'userId' => $params['userId'],
                'view' => '默认错误提示视图 html代码'
            */

            // DB::where('userid', $result['userId'])->update('gzh_open_id', $result['openId']);
            echo $result['view'];
        }
    }

    //[登录二维码] 生成码图
    public function scanLoginImage()
    {;
        $this->instance()->loginQrcodeCreate([
            'redirect' => '/webview/demo/scanLoginValidate'
        ]);
    }

    /**
     * [登录二维码] 微信侧扫描 打开确认页
     */
    public function scanLoginValidate()
    {
        $this->instance()->loginQrcodeValidate('/webview/demo/loginQrcodeConfirm');
    }

    /**
     *  [登录二维码] 微信授权重定向页面
     */
    public function loginQrcodeConfirm()
    {
        $fetchLocalUserIdWithOpenId = function ($mp_user_openId) {
            // TODO 根据用户的公众号openID 查询业务系统是否存在与之关联的用户
            // 本地有用户则返回本地用户ID
            // 本地没有关联用户则返回 布尔假值即可
            return 10001;
        };
        $this->instance()->loginQrcodeConfirm($fetchLocalUserIdWithOpenId);
    }
}
