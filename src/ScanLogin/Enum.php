<?php

/**
 * @name Enum
 * @author vipkwd <service@vipkwd.com>
 * @link https://github.com/wxy545812093/vipkwd-phputils
 * @license http://www.apache.org/licenses/LICENSE-2.0
 * @copyright The PHP-Tools
 */

declare(strict_types=1);

namespace Vipkwd\Utils\ScanLogin;

class Enum
{
    const QR_DATA_DURATION_SECONDS = 3600;
    const QR_PLATFORM_PC = 'pc';
    const QR_PLATFORM_APP = 'app';
    const QR_PLATFORM_OTHER = 'other';

    const QR_STATE_WSCAN = 'wscan';
    const QR_STATE_SCANED = 'complete';
    const QR_STATE_CONFIRM = 'confirm';
    const QR_STATE_AUTH_CONFIRM = 'authConfirm';
    const QR_STATE_AUTH_REJECT = 'authReject';
    const QR_STATE_FAIL = 'fail';
    const QR_STATE_CANCEL = 'cancel';
    const QR_STATE_NORMAL = 'normal';

    static $QR_AES_KEY = '7a57a5a743894a0e';
    static $QR_AES_IV = '1234567890123456';

    const FIXED_KEY_UID = 'scan_uid';
    const FIXED_KEY_CLIENTID = 'clientId';
    const FIXED_KEY_QRCODEID = 'qrcodeId';
    const FIXED_KEY_NOTIFY = 'notify';
    const FIXED_KEY_EVENT_NAME = 'event';
    const FIXED_KEY_EXPIRES_TIME = 'expires';
    const FIXED_KEY_CLIENT_IP = '_cip';
    const FIXED_KEY_IMAGE_CTIME = '_ct';
    const FIXED_KEY_PLATFORM = '_pt';
    const FIXED_KEY_STATE = '_st';
}